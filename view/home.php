<?php
if (!isset($_SESSION['username'])) {
  header("location: index.php");
}

if ($_SESSION['id'] == -1) {
  $disable = "disable";
} else {
  $disable = "";
}

// echo $disable. "      ". $_SESSION['id'];
?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <link href="https://fonts.googleapis.com/css?family=Abril+Fatface|Lobster|PT+Serif&display=swap" rel="stylesheet">
  <script src="https://kit.fontawesome.com/5c4e1169df.js" crossorigin="anonymous"></script>
  <script src="lib/js/home.js"></script>
  <script src="lib/js/material.js"></script>
  <script src="lib/js/projectsHome.js"></script>
  <link rel="stylesheet" href="lib/css/home.css">
  <link rel="stylesheet" href="lib/css/material.css">
  <link rel="stylesheet" href="lib/css/projectsHome.css">
  <link rel="stylesheet" href="lib/css/projects.css">
  <title>Document</title>
</head>

<body>
  <aside class="side">
    <nav class="menu" id="intro">Home</nav>
    <nav class="menu" id="materials">Materials</nav>
    <nav class="menu" id="projects">Projects</nav>
    <nav class="menu <?php echo $disable; ?>" id="feedback<?php echo $disable; ?>">Feedback</nav>
    <nav class="menu <?php echo $disable; ?>" id="contacts<?php echo $disable; ?>">Contacts</nav>
    <nav class="menu logout" id="logout">Logout</nav>
  </aside>

  <div class="main">
    <article id="articleIntro" class="article">
      <h1 class="welcome">Welcome</h1>
      <h3 class="name"><?php echo $_SESSION['username'] ?></h3>
      <h4>to</h4>
      <h1 class="wecode">WeCode</h1>
    </article>

    <article id="articleMaterials" class="article">
      <div class="web">
        <h3 class="tittles">Web Development</h3>
        <img src="img/html.jpg" id="materialHTML" style="width:20%; border-radius: 40px; margin: 2%">
        <img src="img/css.png" id="materialCSS" style="width:20%; border-radius: 40px; margin: 2%">
        <img src="img/js.png" id="materialJS" style="width:20%; border-radius: 40px; margin: 2%">
        <img src="img/php.png" id="materialPHP" style="width:20%; border-radius: 40px; margin: 2%">
        <img src="img/jq.jpg" id="materialJQ" style="width:20%; border-radius: 40px; margin: 2%">
        <img src="img/ajax.png" id="materialAJAX" style="width:20%; border-radius: 40px; margin: 2%">
        <img src="img/boot.jpg" id="materialBOOT" style="width:20%; border-radius: 40px; margin: 2%">
        <img src="img/django.png" id="materialDJ" style="width:20%; border-radius: 40px; margin: 2%">
      </div>
      <div class="software">
        <h3 class="tittles">Software Development</h3>
        <img src="img/c.png" id="materialC" style="width:20%; border-radius: 40px; margin: 2%">
        <img src="img/cpp.png" id="materialCPP" style="width:20%; border-radius: 40px; margin: 2%">
        <img src="img/py.png" id="materialPY" style="width:20%; border-radius: 40px; margin: 2%">
      </div>
      <div class="structure">
        <h3 class="tittles">Programing Structures</h3>
        <img src="img/mvc.jpg" id="materialMVC" style="width:20%; border-radius: 40px; margin: 2%">
        <img src="img/oop.jpg" id="materialOOP" style="width:20%; border-radius: 40px; margin: 2%">
      </div>
      <div class="other">
        <h3 class="tittles">other</h3>
        <img src="img/ard.jpg" id="materialARD" style="width:20%; border-radius: 40px; margin: 2%">
        <img src="img/rasp.jpg" id="materialPI" style="width:20%; border-radius: 40px; margin: 2%">
      </div>
    </article>

    <article id="articleProjects" class="article">
    <h3 class="tittles">School years</h3>
    <nav class="menu-main" id="year2">2017/18</nav>
    <nav class="menu-main" id="year3">2018/19</nav>
    <nav class="menu-main" id="year4">2019/2020</nav>
    </article>

    <article id="articleFeedback" class="article">
      a
    </article>

    <article id="articleContacts" class="article">
      a
    </article>

    <?php include_once("view/materials.php") ?>

    <?php include_once("view/projects2018.php") ?>
    <?php include_once("view/projects2019.php") ?>
    <?php include_once("view/projects2020.php") ?>

  </div>

  <?php include_once("view/projects/NegozioVestiti.php") ?>
</body>

</html>