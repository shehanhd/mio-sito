<script src="lib/js/projects.js"></script>

<article id="proj-NegozioVestiti" class="proj-article project">
    <h1 class="proj-title">Negozio Vestiti</h1>
    <nav class="closebtn proj-closebtn">&times;</nav>

    <div id="add" class="proj-menu">Add</div>
    <div id="edit" class="proj-menu">Edit</div>
    <div id="print" class="proj-menu">Print</div>
    <div id="delete" class="proj-menu">Delete</div>

    <div  class="proj-form" id="form-insert">
        <h1>Insert</h1>
        <form action="index.php" method="post">
            <input type="text" class="proj-input" name="codice" id="codice" pattern="[A-Z]{3}[0-9]{3}" title="es: HDS000" placeholder="Codice Vestiti" required>
            <span>La taglia: </span>
            <select type="text" class="proj-input" style="width:50%" name="taglia" id="taglia">
                <option value="42">42</option>
                <option value="44">44</option>
                <option value="46">46</option>
                <option value="48">48</option>
                <option value="50">50</option>
                <option value="52">52</option>
                <option value="54">54</option>
            </select>
            <input type="number" class="proj-input" name="quantita" id="quantita" placeholder="Quantita" required>
            <input type="number" class="proj-input" name="prezzo" id="prezzo" step="0.01" placeholder="Prezzo" required>
            <input type="button" class="proj-button" id="Aggiungi" value="Submit">
        </form>
    </div>


    <div  class="proj-form" id="form-edit">
        <h1>Edit</h1>
    </div>

    <div  class="proj-form" id="form-print">
        <h1>Print</h1>
    </div>

    <div  class="proj-form" id="form-delete">
        <h1>Delete</h1>
    </div>
</article>