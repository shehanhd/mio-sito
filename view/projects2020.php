<article id="projects2020" class="article material">
    <h1 class="projects-title">Works of 4<sup>th</sup> year</h1>
    <nav class="closebtn">&times;</nav>

    <div class="proj-card">
        <h1>Negozio Vestiti  <i id="negozioVestiti" class="fas fa-external-link-alt proj-link"></i></h1>
        <p class="mat-used">Materials i've used to do this task:</p>
        <ul class="mat-list">
            <li>HTML/CSS/JS/Ajax/jQuery</li>
            <li>PHP/SQL</li>
        </ul>
    </div>
    <!-- <div class="proj-card">
        
    </div> -->
</article>