$(window).on('load',function(){
    $("#year2").click(function () {
        $("#projects2018").show("1");
        $("#year2").addClass("selected-main");
        $("#year3, #year4").removeClass("selected-main");
    });
    $("#year3").click(function () {
        $("#projects2019").show("1");
        $("#year3").addClass("selected-main");
        $("#year2, #year4").removeClass("selected-main");
    });
    $("#year4").click(function () {
        $("#projects2020").show("1");
        $("#year4").addClass("selected-main");
        $("#year2, #year3").removeClass("selected-main");
    });

    $("#negozioVestiti").click(function () {  
        $("#proj-NegozioVestiti").show("1");
    });

    $(".closebtn").click(function () {
        $("#projects2018, #projects2019, #projects2020").hide("1");
        $("#proj-NegozioVestiti").hide("1");
        $("#year4, #year2, #year3").removeClass("selected-main");
    });
});