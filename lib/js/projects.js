$(window).on('load',function(){
    $(".proj-menu").click(function () {
        $(".proj-menu").addClass("proj-menu-verti");
    });

    $("#add").click(function () {
        $("#form-insert").addClass("show");
        $("#form-edit, #form-print, #form-delete").removeClass("show");
    });

    $("#edit").click(function () {
        $("#form-edit").addClass("show");
        $("#form-insert, #form-print, #form-delete").removeClass("show");
    });

    $("#print").click(function () {
        $("#form-print").addClass("show");
        $("#form-insert, #form-delete, #form-edit").removeClass("show");
    });

    $("#delete").click(function () {
        $("#form-delete").addClass("show");
        $("#form-insert, #form-edit, #form-print").removeClass("show");
    });
});