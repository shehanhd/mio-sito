$(window).on('load',function(){
    $("#materialHTML").click(function () {
        $("#contentHTML").show("1");
    });

    $("#materialJS").click(function () {
        $("#contentJS").show("1");
    });

    $("#materialCSS").click(function () {
        $("#contentCSS").show("1");
    });

    $("#materialPHP").click(function () {
        $("#contentPHP").show("1");
    });

    $("#materialJQ").click(function () {
        $("#contentJQ").show("1");
    });

    $("#materialAJAX").click(function () {
        $("#contentAJAX").show("1");
    });

    $("#materialBOOT").click(function () {
        $("#contentBOOT").show("1");
    });

    $("#materialDJ").click(function () {
        $("#contentDJ").show("1");
    });

    $("#materialC").click(function () {
        $("#contentC").show("1");
    });

    $("#materialCPP").click(function () {
        $("#contentCPP").show("1");
    });

    $("#materialPY").click(function () {
        $("#contentPY").show("1");
    });

    $("#materialMVC").click(function () {
        $("#contentMVC").show("1");
    });

    $("#materialOOP").click(function () {
        $("#contentOOP").show("1");
    });

    $("#materialARD").click(function () {
        $("#contentARD").show("1");
    });

    $("#materialPI").click(function () {
        $("#contentPI").show("1");
    });

    $(".closebtn").click(function () {
        $("#contentHTML, #contentJS, #contentCSS, #contentPHP, #contentJQ, #contentAJAX, #contentBOOT, #contentDJ").hide("1");

        $("#contentC, #contentCPP, #contentPY").hide("1");

        $("#contentMVC, #contentOOP").hide("1");

        $("#contentARD, #contentPI").hide("1");
    });

});